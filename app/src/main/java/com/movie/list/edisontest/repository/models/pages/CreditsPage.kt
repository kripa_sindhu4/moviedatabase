package com.movie.list.edisontest.repository.models.pages

import com.movie.list.edisontest.repository.models.Cast

data class CreditsPage(
        val cast: List<Cast>?
)