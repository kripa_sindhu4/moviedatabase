package com.movie.list.edisontest.ui.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.movie.list.edisontest.repository.Repository
import com.movie.list.edisontest.repository.models.Movie
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class MovieDetailsViewModel @Inject constructor(private val repository: Repository) : ViewModel() {

    private var movieLiveData = MutableLiveData<Movie>()
    private val compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        compositeDisposable.clear()
    }

    fun getMovieDetails(movie: Movie?): MutableLiveData<Movie>? {
        if (movie?.id == null) {
            return null
        }

        compositeDisposable.add(
                repository.getMovieDetails(movie.id)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribeBy(
                                onNext = {
                                    movieLiveData.value = it
                                }
                        )
        )

        return movieLiveData
    }

}