package com.movie.list.edisontest.repository.models.pages

import com.movie.list.edisontest.repository.models.Movie

data class MoviePage (
        val results: List<Movie>?
)