package com.movie.list.edisontest.dagger

import android.app.Application
import com.movie.list.edisontest.MoviesApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton


/**
 * The [ApplicationComponent] combines all modules.
 */
@Singleton
@Component(modules = [
    //AndroidInjectionModule::class,
    RepositoryModule::class,
    ActivityBindingModule::class,
    ViewModelModule::class
])
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

    fun inject(moviesApplication: MoviesApplication)
}