package com.movie.list.edisontest.ui.home

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.movie.list.edisontest.Key
import com.movie.list.edisontest.Target
import com.movie.list.edisontest.R
import com.movie.list.edisontest.navigateTo
import com.movie.list.edisontest.repository.models.Movie
import com.movie.list.edisontest.utils.hideKeyboard
import com.movie.list.edisontest.utils.showKeyboard
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_home.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


private const val SPAN_COUNT = 2
private const val SEARCH_DELAY = 500L

class HomeActivity : AppCompatActivity(), MainAdapter.OnInteractionListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var mainAdapter: MainAdapter? = null
    private var mainViewModel: MainViewModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setSupportActionBar(homeToolbar)
        homeToolbar.title = ""

        mainViewModel = ViewModelProviders
                .of(this, viewModelFactory)
                .get(MainViewModel::class.java)

        mainAdapter = MainAdapter(layoutInflater, this)

        homeRv.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(context, SPAN_COUNT)
            adapter = mainAdapter
        }

        mainViewModel?.moviesLiveData
                ?.observe(this, Observer {
                    it?.let {
                        homePb.visibility = View.GONE
                        mainAdapter?.setMovies(it)
                        homeRv.scheduleLayoutAnimation()
                    }
                })

        mainViewModel?.getPopularMovies()

        swipe_to_refresh.setOnRefreshListener {
            mainViewModel?.getPopularMovies()
            mainViewModel?.moviesLiveData
                ?.observe(this, Observer {
                    it?.let {
                        homePb.visibility = View.GONE
                        mainAdapter?.setMovies(it)
                        homeRv.scheduleLayoutAnimation()
                    }
                })
            swipe_to_refresh.isRefreshing = false
        }
    }

    @SuppressLint("CheckResult")
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_home, menu)
        val searchMenuItem = menu.findItem(R.id.searchItem)
        val searchView = searchMenuItem.actionView as SearchView
        // trigger search requests with delay
        createSearchObservable(searchView)
                .debounce(SEARCH_DELAY, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .filter {
                    if (it.isEmpty()) {
                        mainViewModel?.searchMovie(it)
                        false

                    } else {
                        true
                    }
                }
                .distinctUntilChanged()
                .subscribe {
                    mainViewModel?.searchMovie(it)
                }

        return true
    }

    override fun onItemClicked(movie: Movie) {
        val bundle = Bundle()
        bundle.putSerializable(Key.MOVIE, movie)
        navigateTo(this, Target.DETAILS, bundle)
    }

    private fun createSearchObservable(searchView: SearchView): Observable<String> {
        val subject = PublishSubject.create<String>()
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let { subject.onNext(it) }
                hideKeyboard(this@HomeActivity)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let { subject.onNext(it) }
                return true
            }
        })

        return subject
    }

}
