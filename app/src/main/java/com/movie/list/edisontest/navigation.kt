package com.movie.list.edisontest

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.movie.list.edisontest.ui.details.DetailsActivity
import com.movie.list.edisontest.ui.home.HomeActivity

enum class Target {
    ADDITION, DETAILS
}

/**
 * Navigates to the given [target] by starting the corresponding activity [Context]
 */
fun navigateTo(context: Context?, target: Target, bundle: Bundle? = null,
               activityOptions: Bundle? = null) {
    if (context == null) {
        return
    }
    val intent = when (target) {
        Target.DETAILS -> Intent(context, DetailsActivity::class.java)
        else -> Intent(context, HomeActivity::class.java)
    }

    bundle?.let {
        intent.putExtras(it)
    }

    context.startActivity(intent, activityOptions)
}