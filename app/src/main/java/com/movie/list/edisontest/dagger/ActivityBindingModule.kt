package com.movie.list.edisontest.dagger

import com.movie.list.edisontest.ui.details.DetailsActivity
import com.movie.list.edisontest.ui.home.HomeActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector
    abstract fun homeActivity(): HomeActivity

    @ContributesAndroidInjector
    abstract fun detailsActivity(): DetailsActivity

}