package com.movie.list.edisontest.ui.details

import android.os.Bundle
import android.view.MenuItem
import com.movie.list.edisontest.Key
import com.movie.list.edisontest.R
import com.movie.list.edisontest.repository.models.Movie
import com.movie.list.edisontest.utils.getBackdropUrl
import javax.inject.Inject
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.movie.list.edisontest.loadImage
import kotlinx.android.synthetic.main.activity_details.*


class DetailsActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var movie: Movie? = null
    private var detailsAdapter: DetailsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        val detailsViewModel = ViewModelProviders
                .of(this, viewModelFactory)
                .get(MovieDetailsViewModel::class.java)

        movie = if (savedInstanceState != null) {
            savedInstanceState.getSerializable(Key.MOVIE) as Movie
        } else {
            intent.getSerializableExtra(Key.MOVIE) as Movie
        }

        setSupportActionBar(detailsToolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_arrow_back)
            title = ""
        }

        detailsIv.transitionName = getString(R.string.transition_name_details) + movie?.id
        detailsIv.loadImage(getBackdropUrl(movie?.backdropPath))

        setMovieDetails(movie)

        detailsViewModel.getMovieDetails(movie)
                ?.observe(this, Observer {
                    it?.let {
                        setMovieDetails(it)
                    }
                })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putSerializable(Key.MOVIE, movie)
        super.onSaveInstanceState(outState)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        finish()
    }

    private fun setMovieDetails(movie: Movie?) {
        if (detailsAdapter == null) {
            detailsAdapter = DetailsAdapter(layoutInflater)
            detailsRv.layoutManager = LinearLayoutManager(this)
            detailsRv.adapter = detailsAdapter
        }
        detailsAdapter?.setMovie(movie)
    }

}
