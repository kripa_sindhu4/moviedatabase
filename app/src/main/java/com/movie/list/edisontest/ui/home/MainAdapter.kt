package com.movie.list.edisontest.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.movie.list.edisontest.R
import com.movie.list.edisontest.loadImage
import com.movie.list.edisontest.repository.models.Movie
import com.movie.list.edisontest.utils.getPosterUrl

class MainAdapter(private val inflater: LayoutInflater,
                  private val onInteractionListener: OnInteractionListener
) : RecyclerView.Adapter<MainAdapter.HomeViewHolder>() {

    private var movies = listOf<Movie>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            HomeViewHolder(
                    inflater.inflate(R.layout.card_home, parent, false),
                    onInteractionListener
            )

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        holder.bind(movies[position])
    }

    override fun getItemCount() = movies.size

    fun setMovies(movies: List<Movie>) {
        this.movies = movies
        notifyDataSetChanged()
    }

    interface OnInteractionListener {

        fun onItemClicked(movie: Movie)

    }

    class HomeViewHolder(itemView: View, private val onInteractionListener: OnInteractionListener)
        : RecyclerView.ViewHolder(itemView) {

        private val posterIv: ImageView = itemView.findViewById(R.id.homePosterIv)
        private val movieName: TextView = itemView.findViewById(R.id.movieNameTv)

        fun bind(movie: Movie) {
            posterIv.apply {
                loadImage(getPosterUrl(movie.posterPath))
                movieName.text = movie.title

                setOnClickListener {
                    onInteractionListener.onItemClicked(movie)
                }
            }
        }

    }
}