package com.movie.list.edisontest.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.movie.list.edisontest.repository.Repository
import com.movie.list.edisontest.repository.models.Movie
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class MainViewModel
@Inject constructor(private val repository: Repository) : ViewModel() {

    val moviesLiveData = MutableLiveData<List<Movie>>()
    private val compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        compositeDisposable.clear()
    }

    fun searchMovie(query: String) {
        if (query.isEmpty()) {
            getPopularMovies()
            return
        }

        compositeDisposable.add(
                repository.searchMovie(query)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribeBy(
                                onNext = {
                                    moviesLiveData.value = it.results
                                }
                        )
        )
    }

    fun getPopularMovies() {
        compositeDisposable.add(
                repository.getPopularMovies()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribeBy(
                                onNext = {
                                    moviesLiveData.value = it.results
                                }
                        )
        )
    }

}