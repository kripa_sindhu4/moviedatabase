package com.movie.list.edisontest.repository.models

data class Genre(
        val id: String? = null,
        val name: String? = null
)