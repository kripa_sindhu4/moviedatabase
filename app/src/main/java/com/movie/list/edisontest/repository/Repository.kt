package com.movie.list.edisontest.repository


class Repository(private val apiEndPoint: ApiEndPoint) {

    /**
     * Get a list of currently popular movies.
     */
    fun getPopularMovies() = apiEndPoint.getPopularMovies()

    /**
     * Get details to the given movie.
     */
    fun getMovieDetails(id: Long) = apiEndPoint.getMovieDetails(id)

    /**
     * Search all movies for the given query.
     */
    fun searchMovie(query: String) = apiEndPoint.searchMovie(query)
}