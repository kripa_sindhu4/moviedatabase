package com.movie.list.edisontest.repository

import com.movie.list.edisontest.repository.models.Movie
import com.movie.list.edisontest.repository.models.pages.MoviePage
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiEndPoint {

    @GET("movie/popular")
    fun getPopularMovies(): Observable<MoviePage>

    @GET("movie/{movie_id}?append_to_response=credits")
    fun getMovieDetails(@Path(value = "movie_id")movieId: Long): Observable<Movie>

    @GET("search/movie")
    fun searchMovie(@Query("query") query: String): Observable<MoviePage>
}