package com.movie.list.edisontest.dagger

import com.movie.list.edisontest.ui.home.MainViewModel
import dagger.multibindings.IntoMap
import dagger.Binds
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.movie.list.edisontest.ui.details.MovieDetailsViewModel
import dagger.Module

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindHomeViewModel(mainViewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MovieDetailsViewModel::class)
    abstract fun bindDetailsViewModel(movieDetailsViewModel: MovieDetailsViewModel)
            : ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: MovieViewModelFactory)
            : ViewModelProvider.Factory
}